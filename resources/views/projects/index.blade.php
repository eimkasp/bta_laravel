<h1>
    Projektai
</h1>

<table>
    @foreach($projects as $project)
        <tr>
            <td>
                {{ $project->project_name }}
            </td>

            <td>
                {{ $project->year }}
            </td>

            <td>
                {{ $project->price }}
            </td>

            <td>
               <a href="/projektai/{{ $project->id }}">
                   Peržiūrėti
               </a>
            </td>
        </tr>
    @endforeach
</table>